package com.logicsoul.mvvmpattern

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.logicsoul.mvvmpattern.databinding.ActivityMainBinding
import com.logicsoul.mvvmpattern.helper.base.BaseActivity
import com.logicsoul.mvvmpattern.view.recyclerview.UserActivity

/**
 * Landing activity for user
 */
class MainActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding =
            DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)

        binding.btnRvRemoteApi.setOnClickListener {
            UserActivity.startActivity(this)
        }

        //todo : need to impl for room db
        binding.btnRvLocalRoom.setOnClickListener { }
    }
}