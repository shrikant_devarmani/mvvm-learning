package com.logicsoul.mvvmpattern.data.remote

import com.logicsoul.mvvmpattern.helper.constants.ApiConstants.Companion.BASE_URL
import com.logicsoul.mvvmpattern.helper.constants.ApiConstants.Companion.CONNECT_TIMEOUT
import com.logicsoul.mvvmpattern.helper.constants.ApiConstants.Companion.READ_TIMEOUT
import com.logicsoul.mvvmpattern.helper.constants.ApiConstants.Companion.WRITE_TIMEOUT
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * single ton network client
 */
class NetworkClient {
    companion object {
        fun createNetworkClient(): Retrofit {
            val client = OkHttpClient.Builder();
            client.apply {
                connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
                writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
                readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
                retryOnConnectionFailure(true)
                addInterceptor(HttpLoggingInterceptor().apply {
                    level = HttpLoggingInterceptor.Level.BODY
                })

                val retrofit = Retrofit.Builder()
                    .client(client.build())
                    .addConverterFactory(
                        GsonConverterFactory.create()
                    )
                    .baseUrl(BASE_URL)
                    .build()

//                retrofit.create(UserService::class.java)

                return retrofit
            }
        }
    }
}