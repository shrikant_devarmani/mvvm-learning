package com.logicsoul.mvvmpattern.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.logicsoul.mvvmpattern.data.repositories.UserRepository
import com.logicsoul.mvvmpattern.model.User

/**
 *  User view model
 */
class UsersViewModel : ViewModel() {
    private val repository = UserRepository()
    var usersList: LiveData<List<User>>
    val loading = MutableLiveData<Boolean>()

    init {
        usersList = repository.getUsersLiveData()
        getUsers()
    }

    fun getUsers() {
        loading.postValue(true)
        repository.fetchUsers()
    }
}