package com.logicsoul.mvvmpattern.data.remote.api_services

import com.logicsoul.mvvmpattern.model.User
import retrofit2.Call
import retrofit2.http.GET

/**
 * Users related api endpoints here
 */
interface UserService {
    @GET("users/")
    fun users(): Call<List<User>>
}