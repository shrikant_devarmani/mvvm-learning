package com.logicsoul.mvvmpattern.helper.constants

/**
 * App related const here
 */
class AppConstants {
    companion object {
        const val KEY_HELLO = "Hello"
    }
}