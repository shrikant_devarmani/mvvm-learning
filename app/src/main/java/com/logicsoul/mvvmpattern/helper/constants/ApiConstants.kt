package com.logicsoul.mvvmpattern.helper.constants

/**
 * Api related constants here
 */
class ApiConstants {
    companion object {
        const val BASE_URL = "https://jsonplaceholder.typicode.com/"
        const val RESOURCE_SEPERATOR = "/"
        const val RESOURCE_USER = "users$RESOURCE_SEPERATOR"

        const val CONNECT_TIMEOUT = 10L
        const val WRITE_TIMEOUT = 1L
        const val READ_TIMEOUT = 20L
    }
}