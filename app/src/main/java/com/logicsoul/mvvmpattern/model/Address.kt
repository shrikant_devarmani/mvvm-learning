package com.logicsoul.mvvmpattern.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Address(
    val street: String,
    val city: String,
    val zipcode: String,
    val suite: String
) : Parcelable