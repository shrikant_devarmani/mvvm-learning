package com.logicsoul.mvvmpattern.data.repositories

import androidx.lifecycle.MediatorLiveData
import com.logicsoul.mvvmpattern.data.remote.NetworkClient
import com.logicsoul.mvvmpattern.data.remote.api_services.UserService
import com.logicsoul.mvvmpattern.model.User
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * User repo for remote data fetch
 */
class UserRepository {
    private val client = NetworkClient.createNetworkClient()
    private var userService: UserService? = null
    var usersList = MediatorLiveData<List<User>>()

    init {
        userService = client.create(UserService::class.java)
    }

    //fetch from service
    fun fetchUsers() {
        userService?.users()?.enqueue(object : Callback<List<User>> {
            override fun onFailure(call: Call<List<User>>, t: Throwable) {
                print(t.toString())
            }

            override fun onResponse(call: Call<List<User>>, response: Response<List<User>>) {
                if (response.body() != null) {
                    usersList.postValue(response.body())
                    print(response.body().toString())
                }
            }
        })
    }

    //get list
    fun getUsersLiveData(): MediatorLiveData<List<User>> {
        return usersList
    }
}