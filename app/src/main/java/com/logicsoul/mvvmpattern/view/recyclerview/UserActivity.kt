package com.logicsoul.mvvmpattern.view.recyclerview

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.logicsoul.mvvmpattern.R
import com.logicsoul.mvvmpattern.databinding.ActivityUserBinding
import com.logicsoul.mvvmpattern.helper.base.BaseActivity
import com.logicsoul.mvvmpattern.viewmodel.UsersViewModel

/**
 * User list activity
 */
class UserActivity : BaseActivity() {
    companion object {
        fun startActivity(activity: Activity) {
            val intent = Intent(activity, UserActivity::class.java)
            activity.startActivity(intent)
        }
    }

    private var viewModel: UsersViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding =
            DataBindingUtil.setContentView<ActivityUserBinding>(this, R.layout.activity_user)

        viewModel = ViewModelProviders.of(this).get(UsersViewModel::class.java)

        binding.apply {
            lifecycleOwner = this@UserActivity
            viewModel = this@UserActivity.viewModel
        }

        viewModel?.usersList?.observe(this@UserActivity, Observer {
            viewModel?.loading?.value = false
        })
    }
}