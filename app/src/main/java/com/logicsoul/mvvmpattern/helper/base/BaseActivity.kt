package com.logicsoul.mvvmpattern.helper.base

import androidx.appcompat.app.AppCompatActivity

/**
 * Base activity for all activity
 */
abstract class BaseActivity : AppCompatActivity() {
}